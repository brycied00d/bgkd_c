#	$OpenBSD: Makefile,v 1.8 2002/06/08 22:41:46 art Exp $

PROG=	ps
SRCS=	ps.c
DPADD=	${LIBM} ${LIBKVM}
LDADD=	-lkvm -L/usr/local/lib/mysql -lmysqlclient -lz -lm  -lssl -lcrypto

.include <bsd.prog.mk>

