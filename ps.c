/*
	Bryce's bgkd implemented in C
	Demonstration and Test Application
	
	Copyright: Bryce Chidester <brycec@devio.us>
	
	Outline:
		Single-run Mode:
		Obtain list of active users
		Sort list for speed?
		Obtain list of running processes
		1. Check gid of process and skip if !shifty
		2. Check uid of process and skip if ignore_uids
		3. Check p_comm of process and skip if ignore_apps
		4. Look for uid of process in active_users
		4.a If user is not logged in
		4.a.1 kill the process
		4.a.2 Invoke exec() tp send a notification email.
*/

#define VERSION "2.0"

#include <sys/param.h>
#include <sys/user.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/proc.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/sysctl.h>
#include <sys/types.h>
#include <sys/time.h>

#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <kvm.h>
#include <nlist.h>
#include <paths.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <paths.h>
#include <utmp.h>
#include <time.h>
#include <locale.h>
#include <syslog.h>
#include <stdarg.h>
#include <signal.h>

#include <mysql.h>

static int verbose=0;
static int nokill=0;

#define SHIFTY_CLASS "shifty"
#define USER_WHITELIST "whitelist.user"
#define PROC_WHITELIST "whitelist.proc"
#define PROC_BLACKLIST "blacklist.proc"
#define MAX_USERS_LOGGED_IN 1024
char who_list[MAX_USERS_LOGGED_IN][LOGIN_NAME_MAX];
int who_list_size = MAX_USERS_LOGGED_IN;

/* Variables for utmp work */
struct utmp utmp;

MYSQL *mysql;
static char *mysql_config_groups[] = {
	"client",
	"bgkd",
	(char *)NULL
};


int read_utmp(void) {
	FILE *utmp_fd;
	int i;
	
	/* Step 1.1 Open utmp */
	if (!(utmp_fd = fopen(_PATH_UTMP, "r")))
	{
		err(1, "%s", _PATH_UTMP);
		return 1;	/* NOTREACHED */
	}
	/* Step 1.2 Read in utmp into an array */
	for(i=0; fread((char *)&utmp, sizeof(utmp), 1, utmp_fd) == 1; )
	{
		if (*utmp.ut_name && *utmp.ut_line)	/* Only interpret valid, full lines of data */
		{
			//who_list[i] = utmp.ut_name;
			strlcpy(who_list[i], utmp.ut_name, sizeof(who_list[i]));
			syslog(LOG_DEBUG, "UTMP: User: %-*s (idx=%i)", LOGIN_NAME_MAX, who_list[i], i);
			i++;
		}
	}
	fclose(utmp_fd);
	who_list_size=i;
	return 0;
}

char *get_user_loginclass(const char *user)
{
	struct passwd *pw;
	if(strlen(user))
	{
		pw = getpwnam(user);
		return pw->pw_class;
	}
	return "";
}


/* Logical functions - answer a question */
int is_user_logged_in(const char *user)
{
	int i;
	for(i=0; i<who_list_size; i++)
		if(!( strcmp(who_list[i], user) ))
			return 1;
	/* If we reach here, we never found uid so return false */
	return 0;
}

int is_proc_blacklisted(const char *proc)
{
	FILE *fd;
	char buf[KI_MAXCOMLEN];
	if (!(fd = fopen(PROC_BLACKLIST, "r")))
	{
		syslog(LOG_WARNING, "WARNING: Process blacklist file could not be opened.");
		return 0;
	}
	while(fgets(buf, sizeof(buf), fd))
	{
		if(buf[0] == '#')	/* Ignore comments - not that we need this, the # would invalidate strcmp */
			continue;
		if(buf[strlen(buf)-1] == 0x0A)	/* Kill the newline */
			buf[strlen(buf)-1]=0;
		if(!strcmp(buf, proc))
		{
			fclose(fd);
			return 1;
		}
	}
	fclose(fd);
	return 0;
}

int is_proc_whitelisted(const char *proc)
{
	FILE *fd;
	char buf[KI_MAXCOMLEN];
	if (!(fd = fopen(PROC_WHITELIST, "r")))
	{
		syslog(LOG_WARNING, "WARNING: Process whitelist file could not be opened.");
		return 0;
	}
	while(fgets(buf, sizeof(buf), fd))
	{
		if(buf[0] == '#')	/* Ignore comments - not that we need this, the # would invalidate strcmp */
			continue;
		if(buf[strlen(buf)-1] == 0x0A)	/* Kill the newline */
			buf[strlen(buf)-1]=0;
		if(!strcmp(buf, proc))
		{
			fclose(fd);
			return 1;
		}
	}
	fclose(fd);
	return 0;
}

int is_user_whitelisted(const char *user)
{
	FILE *fd;
	char buf[LOGIN_NAME_MAX];
	if (!(fd = fopen(USER_WHITELIST, "r")))
	{
		syslog(LOG_WARNING, "WARNING: User whitelist file could not be opened.");
		return 0;
	}
	while(fgets(buf, sizeof(buf), fd))
	{
		if(buf[0] == '#')	/* Ignore comments - not that we need this, the # would invalidate strcmp */
			continue;
		if(buf[strlen(buf)-1] == 0x0A)	/* Kill the newline */
			buf[strlen(buf)-1]=0;
		if(!strcmp(buf, user))
		{
			fclose(fd);
			return 1;
		}
	}
	fclose(fd);
	return 0;
}

int is_user_loginclass(const char *user, const char *class)
{
	char *u_class;
	if(strlen(user) && strlen(class))
	{
		u_class = get_user_loginclass(user);
		if(!strcmp(u_class, class))
			return 1;
	}
	return 0;
}

int mailuser(const char *user, const char *proc)
{
	char sqlstr[1024] = "";
	char emailaddr[128] = "";
	char emailname[128] = "";
	char emailsubj[128] = "";
	char emailmsg[8192] = "";
	MYSQL_RES *result;
	MYSQL_ROW row;
	
	/* Query the database for the e-mail address */
	snprintf(sqlstr, sizeof(sqlstr), "SELECT `email`,`f_name`,`l_name` FROM `devious`.`users` WHERE `username`='%s'", user);
	if( mysql_query(mysql, sqlstr) )
	{
		syslog(LOG_ERR, "Error executing query \"%s\": %s", sqlstr, mysql_error(mysql));
		return 1;
	}
	if( (result = mysql_store_result(mysql)) == NULL )
	{
		syslog(LOG_ERR, "No results returned while querying for %s's e-mail address.", user);
		return 1;
	}
	if( (row = mysql_fetch_row(result)) == NULL )
	{
		syslog(LOG_ERR, "User %s has no e-mail address?", user);
		return 1;
	}
	snprintf(emailaddr, sizeof(emailaddr), "%s", row[0]);
	snprintf(emailname, sizeof(emailname), "%s %s", row[1], row[2]);
	syslog(LOG_DEBUG, "User: \"%s\" Name: \"%s\" E-mail: \"%s\"", user, emailname, emailaddr);
	mysql_free_result(result);
	
	/* Built the actual email now */
	snprintf(emailsubj, sizeof(emailsubj), "BGKD: Detected background processes");
	snprintf(emailmsg, sizeof(emailmsg), "Dear %s (%s),\r\n\r\nWe have detected the following process "
		"running after you logged out of the system:\r\n\r\n\t%s\r\n\r\nThis process has been killed "
		"and a report has been sent to the admins, as this is a violation of our terms of service."
		"\r\nPlease familiarize yourself with our usage policy http://devio.us/policy with special "
		"emphasis on the background process section.\r\n\r\n---------\r\n\r\nIf you feel that your "
		"process was wrongly killed, please e-mail us directly: admins [at] devio.us.\r\n\r\nWarm "
		"regards,\r\n\r\nBGKD\r\n", emailname, user, proc);
	
	printf("%s", emailmsg);
	return 0;
}


int main(int argc, char *argv[])
{
	/* Variables for kvm_getproc work */
	kvm_t *kd;
	struct kinfo_proc2 *kp, **kinfo;
	/* Slightly more generic */
	int i=0, nentries;
	char errbuf[_POSIX2_LINE_MAX];	/* For our kvm_openfiles failure message */
	int res;
	/* getopt */
	int ch;
	
	while ((ch = getopt(argc, argv, "vn")) != -1)
	{
		switch (ch)
		{
			case 'v':
				verbose++;
				break;
			case 'n':
				nokill++;
				break;
		}
	}
	argc -= optind;
	argv += optind;
	
	openlog("bgkd", LOG_CONS | LOG_PID | (verbose?LOG_PERROR:0), LOG_DAEMON);
	syslog(LOG_INFO, "BGKD v"VERSION" started.");
	
	if(mysql_library_init(0, NULL, mysql_config_groups))
	{
		syslog(LOG_ERR, "Error while initializing MySQL library.");
		exit(1);
	}
	if( (mysql = mysql_init(mysql)) == NULL)
	{
		syslog(LOG_ERR, "Error while initializing MySQL descriptor.");
		exit(1);
	}
	syslog(LOG_DEBUG, "Opening MySQL thread ID: %lu", mysql_thread_id(mysql));
	mysql_options(mysql,MYSQL_READ_DEFAULT_GROUP,"bgkd");
	if( (mysql = mysql_real_connect(mysql, NULL, NULL, NULL, NULL, 0, NULL, 0)) == NULL)
	{
		syslog(LOG_ERR, "Error while connecting to MySQL server.");
		exit(1);
	}
	
	if(mysql_ping(mysql))
	{
		syslog(LOG_ERR, "Error pinging MySQL server.");
		exit(1);
	}
	syslog(LOG_DEBUG, "Post-ping MySQL thread ID: %lu", mysql_thread_id(mysql));
	
	/* XXX */
	mailuser("brycec", "I like pie.");
	goto end;
	/* XXX */
	
	/* Step 1. Build a list of active users */
	if(( res=read_utmp() ))
	{	/* We probably would never end up here, the err() in read_utmp() takes care of that */
		syslog(LOG_ERR, "Error reading in utmp, DIEING.");
		return 1;
	}
	syslog(LOG_INFO, "There are currently %i sessions open.", who_list_size);
	
	/* Step 2 Get list of running processes and process it */
	kd = kvm_openfiles(NULL, NULL, NULL, KVM_NO_FILES, errbuf);
	if (kd == NULL)
		errx(1, "%s", errbuf);

	kp = kvm_getproc2(kd, KERN_PROC_ALL, 0, sizeof(*kp), &nentries);
	if (kp == NULL)
		errx(1, "%s", kvm_geterr(kd));
	syslog(LOG_INFO, "There are %i active processes.", nentries);
	
	if (nentries == 0)	/* Die if we didn't get any results back (weird!) */
		exit(1);
	
	if ((kinfo = calloc(sizeof(*kinfo), nentries)) == NULL)
		err(1, "failed to allocate memory for proc pointers");
	for (i = 0; i < nentries; i++)
		kinfo[i] = &kp[i];
	
	for (i = 0; i < nentries; i++)	/* XXX */
	{
		char **myargv, **p;
		char cmdline[3060] = "";
		char datestr[32];
		char logbuf[4096] = "";
		char mailstr[4096] = "";
		int killproc = -1;	/* Tri-state: -1=unset/default, 0=skip, 1=kill */
		
		/* This is the "core" logic */
		/* TODO: Add a way to "blacklist" a process, kill it no matter who owns it */
		/* n.b. This logic is ordered to best filter the data expected */
		if(kinfo[i]->p_ustart_sec)
			strftime(datestr, sizeof(datestr), "%F %T %z", gmtime(&kinfo[i]->p_ustart_sec));
		else
			snprintf(datestr, sizeof(datestr), "");
		
		snprintf(logbuf, sizeof(logbuf), "[%i] (%s) (%s) (%s) (%s) " ,
			kinfo[i]->p_pid, kinfo[i]->p_login,
			get_user_loginclass(kinfo[i]->p_login),
			datestr, kinfo[i]->p_comm);
		myargv = kvm_getargv2(kd, kinfo[i], 4096);
		if ((p = myargv) != NULL) {
			while (*p) {
				strlcat(cmdline, *p, sizeof(cmdline));
				p++;
				if (*p)
					strlcat(cmdline, " ", sizeof(cmdline));
			}
		}
		
		if(killproc < 0 && is_proc_blacklisted(kinfo[i]->p_comm))
		{
			snprintf(logbuf, sizeof(logbuf), "%sKILL \"Process is blacklisted.\" ", logbuf);
			killproc=1;	/* kill */
		}
		if(killproc < 0 && !is_user_loginclass(kinfo[i]->p_login, SHIFTY_CLASS))
		{
			snprintf(logbuf, sizeof(logbuf), "%sSKIP \"User is not in class '"SHIFTY_CLASS"'\" ", logbuf);
			killproc=0;	/* skip */
		}
		if(killproc < 0 && is_user_logged_in(kinfo[i]->p_login))
		{
			snprintf(logbuf, sizeof(logbuf), "%sSKIP \"User is logged in.\" ", logbuf);
			killproc=0;	/* skip */
		}
		if(killproc < 0 && is_proc_whitelisted(kinfo[i]->p_comm))
		{
			snprintf(logbuf, sizeof(logbuf), "%sSKIP \"Process is whitelisted.\" ", logbuf);
			killproc=0;	/* skip */
		}
		if(killproc < 0 && is_user_whitelisted(kinfo[i]->p_login))
		{
			snprintf(logbuf, sizeof(logbuf), "%sSKIP \"User is whitelisted.\" ", logbuf);
			killproc=0;	/* skip */
		}
		
		if(killproc == -1)
			snprintf(logbuf, sizeof(logbuf), "%sKILL \"User is not logged in.\" ", logbuf);
		snprintf(logbuf, sizeof(logbuf), "%s%s", logbuf, cmdline);
		
		if(killproc)
		{
			syslog(LOG_INFO, "%s", logbuf);
			if(!nokill)
			{
				if(kill(kinfo[i]->p_pid, SIGKILL))
				{
					syslog(LOG_ERR, "Error killing PID %i: (%i) %s", kinfo[i]->p_pid, errno, strerror(errno));
				}
			}
			snprintf(mailstr, sizeof(mailstr), "%i: %s", kinfo[i]->p_pid, cmdline);
			mailuser(kinfo[i]->p_login, mailstr);
		} else
		{
			// Non-KILL messages are just debug.
			syslog(LOG_DEBUG, "%s", logbuf);
		}
	}
	
end:
	mysql_close(mysql);
	mysql_library_end();
	
	exit(0);
}
